#include <avr/interrupt.h>
#include "mmouse_functions.h"

#ifndef TESTS_INCLUDED
#define TESTS_INCLUDED

void test_motors();
void test_motors_smooth();
void test_button();
void test_sensors();
void test_leds();
void choose_sensor();

#endif