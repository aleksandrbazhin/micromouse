﻿#include "sharp_sensors.h"
#include <math.h>

float convert_sharp_value(uint16_t adc_value) {
    //constrain within max min
    if(adc_value < SHARP_VAL_MIN) {
        adc_value = SHARP_VAL_MIN;
    }
    else if(adc_value > SHARP_VAL_MAX) {
        adc_value = SHARP_VAL_MAX;
    }
    return SHARP_FIT_COEF_C * pow(adc_value, SHARP_FIT_COEF_P);
}