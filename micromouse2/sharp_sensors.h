// library for GP2Y0A51SK0F_5V sensor
// port from arduino library
// https://github.com/DrGFreeman/SharpDistSensor

#include <avr/io.h>

#ifndef SHARP_INCLUDED
#define SHARP_INCLUDED

#define SHARP_VAL_MIN 70
#define SHARP_VAL_MAX 550
#define SHARP_FIT_COEF_P -1.26093
#define SHARP_FIT_COEF_C 4.03576E+4

float convert_sharp_value(uint16_t adc_value);

#endif
