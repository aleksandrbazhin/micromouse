/*
 * micromouse.cpp
 *
 * Created: 26.05.2018 16:26:26
 * Author : user
 */ 

#define F_CPU 8000000UL
#include <avr/io.h>
#include <avr/portpins.h>
#include <avr/interrupt.h>
#include <util/delay.h>
#include "mmouse_functions.h"
#include <math.h>

// include tests only in debug (does not seem to work)
#ifndef NDEBUG 
#include "tests.h"
#endif

#define CELL_FORWARD 0b00000001
#define CELL_LEFT 0b00000010
#define CELL_RIGHT 0b00000100
#define CELL_LEFT_RIGHT 0b00000110
#define CELL_FORWARD_LEFT 0b00000011
#define CELL_FORWARD_RIGHT 0b00000101
#define CELL_FORWARD_LEFT_RIGHT 0b00000111
#define CELL_STOP 0b00000000
#define CELL_UNKNOWN 0b11111111

#define POS_CELL_BEGINNING 0
#define POS_CELL_CENTER 1

#define WALL_DIST_SIDE 110
#define WALL_DIST_FORWARD 80

#define REGUL_P 0.28
#define REGUL_D 0.73
#define REGUL_SPEED_COEFF 0.01
#define COEF_MOTOR_RIGHT 1.1
#define REGUL_WALL_DISTANCE 75

#define REGULATOR_BOTH 0
#define REGULATOR_LEFT 1
#define REGULATOR_RIGHT 2

#define SLOW_SPEED 80
#define MS_TO_CELL_BEGINNING 250
#define MS_TO_CELL_CENTER 250
#define MS_TO_CELL_BEGINNING_REG 180
#define MS_TO_CELL_CENTER_REG 180

#define MOVE_TYPE_BEGINNING 0
#define MOVE_TYPE_CENTER 1

#define ERR_DELTA 15
#define STALL_COUNT_MAX 110

uint8_t compute_cell_type(const distances_t &distances, const uint8_t self_pos, uint8_t current_cell_type) {
    if (current_cell_type == CELL_UNKNOWN) {
        current_cell_type = CELL_STOP;
    }
    if (self_pos == POS_CELL_BEGINNING) {
        if (distances.dist_l_l > WALL_DIST_SIDE && distances.dist_r_r > WALL_DIST_SIDE) {
            //set_led(LED_3, ON);
            //set_led(LED_4, ON);
            return CELL_LEFT_RIGHT;
        } else if (distances.dist_l_l > WALL_DIST_SIDE) {
            //set_led(LED_3, ON);
            //set_led(LED_4, OFF);
            return CELL_LEFT;
        } else if (distances.dist_r_r > WALL_DIST_SIDE) {
            //set_led(LED_3, OFF);
            //set_led(LED_4, ON);
            return CELL_RIGHT;
        } else {
            //set_led(LED_3, OFF);
            //set_led(LED_4, OFF);
            return CELL_STOP;
        }
    } else if (self_pos == POS_CELL_CENTER) {
        if (distances.dist_l_f > WALL_DIST_FORWARD && distances.dist_r_f > WALL_DIST_FORWARD) {
            //set_led(LED_2, ON);
            return current_cell_type | CELL_FORWARD;
        } else {
            //set_led(LED_2, OFF);
            return current_cell_type;
        }
    } else {
        return CELL_UNKNOWN;
    }
}


void straight_move_regulator_for(const uint16_t ms, 
                                 const int16_t speed, 
                                 const uint8_t reg_type) {
    uint16_t count_millis = 0;
    distances_t distances;
    float err=0, prev_err=0;
    int16_t regulator_value;
    set_led(LED_4, ON);
    while(count_millis < ms) {
        distances = get_all_distances();
        count_millis += SENSOR_DELAY_MS * SENSOR_FILTER_LEN;
        switch (reg_type) {
            case REGULATOR_LEFT:
                err = distances.dist_l_l - REGUL_WALL_DISTANCE;
                break;
            case REGULATOR_RIGHT:
                err = REGUL_WALL_DISTANCE - distances.dist_r_r;
                break;
            case REGULATOR_BOTH:
            default:
                err = distances.dist_l_l - distances.dist_r_r;
        }
        regulator_value = err * REGUL_P - (prev_err - err) * REGUL_D;
        regulator_value *= REGUL_SPEED_COEFF * speed;
        set_motor(MOTOR_LEFT, speed - regulator_value);
        set_motor(MOTOR_RIGHT, COEF_MOTOR_RIGHT * (speed + regulator_value));
    }
    set_led(LED_4, OFF);
}

void move_in_cell(uint8_t cell_type, 
                  const int16_t speed, 
                  const uint8_t move_type) {
    uint16_t reg_time = (move_type == MOVE_TYPE_BEGINNING) ? MS_TO_CELL_BEGINNING_REG : MS_TO_CELL_CENTER_REG;
    if (cell_type == CELL_UNKNOWN && move_type == MOVE_TYPE_BEGINNING) {
        cell_type = CELL_FORWARD;
    }
    switch (cell_type) {
        case CELL_FORWARD:
        case CELL_STOP:
            straight_move_regulator_for(reg_time, speed, REGULATOR_LEFT);
            break;
        case CELL_FORWARD_LEFT:
        case CELL_LEFT:
            straight_move_regulator_for(reg_time, speed, REGULATOR_LEFT);
            break;
        case CELL_FORWARD_RIGHT:
        case CELL_RIGHT:
            straight_move_regulator_for(reg_time, speed, REGULATOR_LEFT);
            break;
        case CELL_FORWARD_LEFT_RIGHT:
            set_led(LED_3, ON);
            set_motor(MOTOR_LEFT, SLOW_SPEED);
            set_motor(MOTOR_RIGHT * COEF_MOTOR_RIGHT, SLOW_SPEED);
            if (move_type == MOVE_TYPE_BEGINNING) {
                _delay_ms(MS_TO_CELL_BEGINNING);
            } else {
                _delay_ms(MS_TO_CELL_CENTER);
            }
            set_led(LED_3, OFF);
        default: 
            set_led(LED_2, ON);
            _delay_ms(300);
            set_led(LED_2, OFF);
    }
}

void move_cell_to_beginning(const uint8_t prev_cell_type) {
    move_in_cell(prev_cell_type, SLOW_SPEED, MOVE_TYPE_BEGINNING);
    
}

void move_cell_to_center(const uint8_t curr_cell_type) {
    move_in_cell(curr_cell_type, SLOW_SPEED, MOVE_TYPE_CENTER);
}

void turn_left() {
    set_motor(MOTOR_LEFT, -150);
    set_motor(MOTOR_RIGHT, 150);
    _delay_ms(300);
}

void turn_right() {
    set_motor(MOTOR_LEFT, 150);
    set_motor(MOTOR_RIGHT, -150);
    _delay_ms(300);
}

void move_cell_by_type(const uint8_t next_cell_type) {
    switch (next_cell_type) {
        case CELL_FORWARD:
            break;
        case CELL_RIGHT:
            turn_right();
            break;
        case CELL_LEFT:
            turn_left();
            break;
        case CELL_STOP:
            turn_right();
            turn_right();
            break;
        default:
            break;
    }
}

void move_random_regulator() {
     uint16_t count_millis = 0;
     distances_t distances;
     float err=0, prev_err=0, prev_err_for_stall_1 = 0, 
        prev_err_for_stall_2 = 0, err_for_stall_1 = 0, err_for_stall_2 = 0;
     int16_t regulator_value = 0, speed = SLOW_SPEED;
     uint8_t reg_type = REGULATOR_LEFT;
     set_led(LED_4, ON);
     uint16_t stall_count = 0;
     while(1) {
         distances = get_all_distances();
         count_millis += SENSOR_DELAY_MS * SENSOR_FILTER_LEN;
         switch (reg_type) {
             case REGULATOR_LEFT:
             err = distances.dist_l_l - REGUL_WALL_DISTANCE;
             break;
             case REGULATOR_RIGHT:
             err = REGUL_WALL_DISTANCE - distances.dist_r_r;
             break;
             case REGULATOR_BOTH:
             default:
             err = distances.dist_l_l - distances.dist_r_r;
         }
         if (distances.dist_l_f < WALL_DIST_FORWARD && distances.dist_r_f < WALL_DIST_FORWARD) {
            uint8_t cell_type = compute_cell_type(distances, POS_CELL_BEGINNING, CELL_UNKNOWN);
            switch (cell_type) {
                case CELL_RIGHT:
                turn_right();
                break;
                case CELL_LEFT:
                turn_left();
                break;
                case CELL_STOP:
                set_motor(MOTOR_LEFT, -150);
                set_motor(MOTOR_RIGHT, -150);
                _delay_ms(200);
                turn_right();
                turn_right();
                break;
            }
         }
         err_for_stall_1 = distances.dist_l_l + distances.dist_r_r;
         err_for_stall_2 = distances.dist_l_f + distances.dist_r_f;
         if (fabs(prev_err_for_stall_1 - err_for_stall_1) < ERR_DELTA && 
            fabs(prev_err_for_stall_2 - err_for_stall_2) < ERR_DELTA) {
            stall_count += 1;
         } else {
            stall_count = 0;
         }
         prev_err_for_stall_1 = err_for_stall_1;
         prev_err_for_stall_2 = err_for_stall_2;
         if (stall_count > STALL_COUNT_MAX) {
            set_motor(MOTOR_LEFT, -200);
            set_motor(MOTOR_RIGHT, -200);
            _delay_ms(200);
         }
         regulator_value = err * REGUL_P - (prev_err - err) * REGUL_D;
         regulator_value *= REGUL_SPEED_COEFF * speed;
         set_motor(MOTOR_LEFT, speed - regulator_value);
         set_motor(MOTOR_RIGHT, COEF_MOTOR_RIGHT * (speed + regulator_value));
         if (count_millis > 5000) {
            if (reg_type == REGULATOR_LEFT) {
                reg_type = REGULATOR_RIGHT;
            } else {
                reg_type = REGULATOR_LEFT;
            }
            count_millis = 0;
         }
     }
     set_led(LED_4, OFF);
}

uint8_t path_exploring_step_choose(const uint8_t cell_type, const uint8_t pos_x, const uint8_t pos_y) {
    if (cell_type == CELL_FORWARD_LEFT || cell_type == CELL_FORWARD_RIGHT || cell_type == CELL_FORWARD_LEFT_RIGHT) {
        return CELL_FORWARD;
    } else {
        return CELL_LEFT;
    }
}

uint8_t choose_cell(const uint8_t cell_type, const uint8_t pos_x, const uint8_t pos_y) {
    if (cell_type == CELL_FORWARD || cell_type == CELL_LEFT || cell_type == CELL_RIGHT || CELL_STOP) {
        return cell_type;
    } else {
        return path_exploring_step_choose(cell_type, pos_x, pos_y);
    }  
}

void stop() {
    set_motor(MOTOR_LEFT, 0);
    set_motor(MOTOR_RIGHT, 0);
    _delay_ms(150);
}


void run_algorithm() {
    init_signal();
    while (1) {
    move_random_regulator();
    }
    int toggle = 0, press = 0;
    //uint8_t pos_x = 0, pos_y = 0;
    //uint8_t prev_cell_type = CELL_UNKNOWN;
    /*while (1) {
        if (button_pressed()) {
            if (press == 0) {
                toggle ^= 1;
                init_signal();
            }
            press = 1;
        } else {
            press = 0;
        }
        if (toggle) {
            move_random_regulator();
            //move_cell_to_beginning(prev_cell_type);
            //stop();
            //distances_t all_distances = get_all_distances();
            //uint8_t cell_type = compute_cell_type(all_distances, POS_CELL_BEGINNING, CELL_UNKNOWN);
            //
            //move_cell_to_center(cell_type);
            //stop();
//
            //all_distances = get_all_distances();
            //cell_type = compute_cell_type(all_distances, POS_CELL_CENTER, cell_type);
//
            //move_cell_by_type(choose_cell(cell_type, pos_x, pos_y));
            //stop();
//
            //prev_cell_type = cell_type;
        }
    }
*/
}

int main(void)
{
    init_mouse();
    //init_signal();
    //run_algorithm();
    test_sensors();
    //test_motors();
    //choose_sensor();
}