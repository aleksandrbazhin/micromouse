#include <avr/io.h>
#include <avr/portpins.h>
#include <avr/interrupt.h>
#include "sharp_sensors.h"

#ifndef FUNCTIONS_INCLUDED
#define FUNCTIONS_INCLUDED

#define MOTOR_LEFT 0
#define MOTOR_RIGHT 1

#define SENSOR_LEFT_FWD 0
#define SENSOR_LEFT_45 1
#define SENSOR_RIGHT_45 2
#define SENSOR_RIGHT_FWD 3

#define LED_1 0
#define LED_2 1
#define LED_3 2
#define LED_4 3
#define OFF 0
#define ON 1

#define SENSOR_FILTER_LEN 4
#define SENSOR_DELAY_MS 2

void init_mouse();

struct distances_t {
    float dist_l_l;
    float dist_r_r;
    float dist_l_f;
    float dist_r_f;
};

// speed is -255 to 255
uint8_t set_motor(const uint8_t motor, int16_t speed);
float get_distance(const uint8_t sensor);
distances_t get_all_distances();
uint8_t button_pressed();
void set_led(const uint8_t led, const uint8_t state);
void init_signal();
uint16_t read_sensor_adc_value(const uint8_t sensor);

#endif