﻿#define F_CPU 8000000UL
#include <util/delay.h>
#include "tests.h"

void test_motors() {
    set_motor(MOTOR_RIGHT, 100);
    set_motor(MOTOR_LEFT, 100);
    _delay_ms(300);
    set_motor(MOTOR_RIGHT, -100);
    set_motor(MOTOR_LEFT, -100);
    _delay_ms(300);
    set_motor(MOTOR_RIGHT, 255);
    set_motor(MOTOR_LEFT, 255);
    _delay_ms(300);
    set_motor(MOTOR_RIGHT, 0);
    set_motor(MOTOR_LEFT, 0);
}

void test_motors_smooth() {
    int spd = 10, inc = 10;
    while (1) {
        if (spd > 255 || spd < -255) {
            inc = -inc;
        }
        spd += inc;
        set_motor(MOTOR_RIGHT, spd);
        set_motor(MOTOR_LEFT, spd);
        _delay_ms(50);
    }
}

void test_button() {
    int toggle=0;
    while (1) {
        if (button_pressed()) {
            toggle ^= 1;
            if (toggle) {
                set_motor(MOTOR_LEFT, 200);
                set_motor(MOTOR_RIGHT, 200);
                } else {
                set_motor(MOTOR_LEFT, 0);
                set_motor(MOTOR_RIGHT, 0);
            }
        }
        _delay_ms(100);
    }
}

void test_sensors() {
    int toggle = 0, press = 0;
    while (1) {
        if (button_pressed()) {
            if (press == 0) {
                toggle ^= 1;       
            }
            press = 1;
        } else {
            press = 0;
        }
        if (toggle) {
            //uint16_t dist = get_distance(SENSOR_LEFT_FWD);
            //uint16_t dist = get_distance(SENSOR_LEFT_45);
            //uint16_t dist = get_distance(SENSOR_RIGHT_FWD);
            uint16_t dist = get_distance(SENSOR_RIGHT_45);
            int16_t spd = 0;
            if (dist < 75) {
                spd = -255;
                set_led(LED_4, ON);
                set_led(LED_3, OFF);
            } else if (dist > 140) {
                spd = 255;
                set_led(LED_3, ON);
                set_led(LED_4, OFF);
            } else {
                spd = dist*2-70;
                set_led(LED_3, OFF);
                set_led(LED_4, OFF);
            }
            set_motor(MOTOR_LEFT, spd);
            set_motor(MOTOR_RIGHT, spd);
        } else {
            set_motor(MOTOR_LEFT, 0);
            set_motor(MOTOR_RIGHT, 0);
        }
        _delay_ms(10);
    }
}

void test_leds() {
    while (1) {
        set_led(LED_1, ON);
        set_led(LED_2, ON);
        set_led(LED_3, ON);
        set_led(LED_4, ON);
        _delay_ms(500);
        set_led(LED_1, OFF);
        set_led(LED_2, OFF);
        set_led(LED_3, OFF);
        set_led(LED_4, OFF);
        _delay_ms(500);
    }
}

void choose_sensor() {
    while(1) {
        uint16_t s = read_sensor_adc_value(SENSOR_RIGHT_FWD);
        set_motor(MOTOR_RIGHT, s/2);
        uint16_t f = read_sensor_adc_value(SENSOR_LEFT_FWD);
        set_motor(MOTOR_RIGHT, f/2);
    }
}