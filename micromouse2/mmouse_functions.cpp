﻿#define F_CPU 8000000UL
#include <util/delay.h>
#include "mmouse_functions.h"

void init_mouse() {
    DDRE |= (1 << PE3); // left motor A
    DDRE |= (1 << PE4); // left motor B
    DDRB |= (1 << PB5); // right motor A
    DDRB |= (1 << PB6); // right motor B

    // timers pwm setup for motors
    cli();
    TCCR3A |= (1 << COM3A1) | (1 << COM3B1); // non-inverting mode
    TCCR3A |= (1 << WGM30); // fast-pwm 8-bit counter Abit
    TCCR3B |= (1 << WGM32); // fast-pwm 8-bit counter Bbit
    TCCR3B |= (1 << CS32); // 256 prescaler
    TCCR1A |= (1 << COM1A1) | (1 << COM1B1);
    TCCR1A |= (1 << WGM10);
    TCCR1B |= (1 << WGM12);
    TCCR1B |= (1 << CS12);
    sei();

    DDRE &= ~(1 << PE7); // button

    DDRC |= (1 << PC0); // led 0
    DDRC |= (1 << PC1); // led 1
    DDRC |= (1 << PC2); // led 2
    DDRC |= (1 << PC3); // led 3

    // ADC setup
    ADCSRA |= (1 << ADPS2) | (1 << ADPS1) | (1 << ADPS0); // 128 prescaler
    ADCSRA |= (1 << ADEN); // enable adc
    
}
// speed is -255 to 255
uint8_t set_motor(const uint8_t motor, int16_t speed) {
    if (speed > 255) {
        speed = 255;
    }
    if (speed < -255) {
        speed = -255;
    }

// gray mouse
    if (motor == MOTOR_LEFT) {
        if (speed > 0) {
            OCR3A = 0;
            OCR3B = speed;
            return 1;
        } else if (speed < 0) {
            OCR3A = -speed;
            OCR3B = 0;
            return 1;
        } else {
            OCR3A = 255;
            OCR3B = 255;
        }
    } 
    else if (motor == MOTOR_RIGHT) {
        if (speed >= 0) {
            OCR1A = speed;
            OCR1B = 0;
            return 1;
        } else if (speed < 0) {
            OCR1A = 0;
            OCR1B = -speed;
            return 1;
        } else {
            OCR1A = 255;
            OCR1B = 255;            
        }
    }
    return 0;
}

// returns 0-1024 on success
uint16_t read_sensor_adc_value(const uint8_t sensor) {
    uint8_t channel = 0;
    
    switch (sensor) {
        case SENSOR_LEFT_45:
            channel |= (1 << MUX0); // ADC2
            break;
        break;
            case SENSOR_LEFT_FWD: // ADC0
            break;
        case SENSOR_RIGHT_45: // ADC3
            channel |= (1 << MUX1);
            break;
        case SENSOR_RIGHT_FWD: // ADC1
            channel |= (1 << MUX0) | (1 << MUX1);
            break;
        default:
            return 0;
    }
    
    ADMUX = (ADMUX & ~(0b00000111)) | channel; // clear mux and add channel
    ADCSRA |= (1 << ADSC); // start conversion
    while (ADCSRA & (1 << ADSC)); // wait for conversion
    return (ADC);
}

distances_t get_all_distances() {
    uint16_t sensor_value_accum[4] = {0, 0, 0, 0};
    distances_t distances;
    for(uint8_t i = 0; i < SENSOR_FILTER_LEN; i++) {
        sensor_value_accum[0] += read_sensor_adc_value(SENSOR_LEFT_45);
        sensor_value_accum[1] += read_sensor_adc_value(SENSOR_RIGHT_45);
        sensor_value_accum[2] += read_sensor_adc_value(SENSOR_LEFT_FWD);
        sensor_value_accum[3] += read_sensor_adc_value(SENSOR_RIGHT_FWD);
        _delay_ms(SENSOR_DELAY_MS);
    }
    distances.dist_l_l = convert_sharp_value(sensor_value_accum[0] / SENSOR_FILTER_LEN);
    distances.dist_r_r = convert_sharp_value(sensor_value_accum[1] / SENSOR_FILTER_LEN);
    distances.dist_l_f = convert_sharp_value(sensor_value_accum[2] / SENSOR_FILTER_LEN);
    distances.dist_r_f = convert_sharp_value(sensor_value_accum[3] / SENSOR_FILTER_LEN);
    return distances;
}

float get_distance(const uint8_t sensor) {
    return convert_sharp_value(read_sensor_adc_value(sensor) / SENSOR_FILTER_LEN);
}

uint8_t button_pressed() {
    return !(PINE & (1 << PE7)); // with external pullup
}

void set_led(const uint8_t led, const uint8_t state) {
    switch (led) {
        case LED_1:
            if (state == ON) {
                PORTC |= (1 << PC0);
            } else {
                PORTC &= ~(1 << PC0);
            }
            break;
        case LED_2:
            if (state == ON) {
                PORTC |= (1 << PC1);
                } else {
                PORTC &= ~(1 << PC1);
            }
            break;
        case LED_3:
            if (state == ON) {
                PORTC |= (1 << PC2);
                } else {
                PORTC &= ~(1 << PC2);
            }
            break;
        case LED_4:
            if (state == ON) {
                PORTC |= (1 << PC3);
                } else {
                PORTC &= ~(1 << PC3);
            }
            break;
    }
}


// led signal on button press
void init_signal() {
    set_led(LED_1, ON);
    _delay_ms(100);
    set_led(LED_2, ON);
    _delay_ms(100);
    set_led(LED_3, ON);
    _delay_ms(100);
    set_led(LED_4, ON);
    _delay_ms(100);
    set_led(LED_1, OFF);
    set_led(LED_2, OFF);
    set_led(LED_3, OFF);
    set_led(LED_4, OFF);
    _delay_ms(50);
    set_led(LED_1, ON);
    set_led(LED_2, ON);
    set_led(LED_3, ON);
    set_led(LED_4, ON);
    _delay_ms(100);
    set_led(LED_1, OFF);
    set_led(LED_2, OFF);
    set_led(LED_3, OFF);
    set_led(LED_4, OFF);
}